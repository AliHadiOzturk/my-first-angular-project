import { Component, OnInit } from '@angular/core';
import { Address } from "../../models/addresses";
import { AddressService } from "../../services/address.service";

@Component({
  selector: 'app-address-list',
  templateUrl: './address-list.component.html'
})
export class AddressListComponent implements OnInit {

  InternationalId:string;
  public adres:Address=new Address;
  adresler:Address[];
  all=true;
  constructor(public addressServices:AddressService) { }
  ngOnInit() {
  }
  startSearch(){
    console.log("Searching for",this.InternationalId);
    if(this.InternationalId!=null){
      this.startSearchByInternationalId(this.InternationalId);
    }
    else{
      this.GetAllData()
    }
  }
  
  GetAllData(){
    this.adresler=this.addressServices.getAll();
    this.all=false;
  }
  startSearchByInternationalId(id:string){
    this.adres=this.addressServices.getAddress(id);
    this.all=true;
    this.InternationalId=null;
  }

}
