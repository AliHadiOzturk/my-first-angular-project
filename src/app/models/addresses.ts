export class Address{
    internationalId:string;
    name:string;
    surname:string;
    phonenumber:string;
    email:string;
    addressline:string;
}