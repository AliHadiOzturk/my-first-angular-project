import { Injectable } from '@angular/core';
import { Address } from "../models/addresses";
import { Adresler } from '../components/adresler'
@Injectable()
export class AddressService {

  constructor(public address:Address=new Address) { }
  getAddress(id:string){
      for(let item of Adresler)
      {
        if(item.internationalId==id){
          this.address=item;
          return this.address;
        }
      }
    }
    getAll(){
      return Adresler;
    }
  }
