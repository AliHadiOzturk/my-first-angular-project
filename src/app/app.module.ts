import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { AddressListComponent } from './components/address-list/address-list.component';
import { FormsModule } from "@angular/forms";
import { AddressService } from './services/address.service';
import { Address } from './models/addresses';

@NgModule({
  declarations: [
    AppComponent,
    AddressListComponent
  ],
  imports: [
    BrowserModule,
    FormsModule
  ],
  providers: [AddressService,Address],
  bootstrap: [AppComponent]
})
export class AppModule { }
